import { modelOptions, prop, Ref } from '@typegoose/typegoose';
import { Encounter, Patient } from '../../../administration';
import { EntityBase, Uuid } from '../../../common';
import { ChiefComplaintForms } from './ChiefComplaintForms';

@modelOptions({
  schemaOptions: {
    collation: { locale: 'en' },
    collection: 'chief_complaint',
  },
})
export class ChiefComplaint extends EntityBase {
  @prop({ required: true, ref: 'Encounter', type: () => String, index: true })
  encounterRef!: Ref<Encounter, Uuid>;

  @prop({ required: true, ref: 'Patient', type: () => String, index: true })
  patientRef!: Ref<Patient, Uuid>;

  @prop()
  text?: string;

  @prop()
  esiLevel?: string;

  @prop({ required: true })
  richText!: string;

  @prop()
  remark?: string;

  @prop()
  isFromDoctor?: boolean;

  @prop()
  formsValues?: ChiefComplaintForms[];
}
