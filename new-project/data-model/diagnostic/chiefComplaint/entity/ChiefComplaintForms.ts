import { prop } from '@typegoose/typegoose';
import { EntityBase } from '../../../common';

export class ChiefComplaintForms extends EntityBase {
  @prop({ required: true })
  type!: string;

  @prop()
  value?: string;

  @prop()
  amount?: string;

  @prop()
  duration?: string;
}
