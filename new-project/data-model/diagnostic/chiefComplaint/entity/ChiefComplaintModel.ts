import { getModelForClass } from '@typegoose/typegoose';
import { ChiefComplaint } from './ChiefComplaint';

export const ChiefComplaintModel = getModelForClass(ChiefComplaint);
