import { getModelForClass } from '@typegoose/typegoose';
import { ClientSession } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
import aggregatePaginate from 'mongoose-aggregate-paginate-v2';
import { IPagination } from '../repository.interface';
import { EntityNotFoundError } from '../../error';
import { Uuid } from '../../type';
import {
  IMongooseAggregatePaginationOptions,
  IMongoosePaginationOptions,
  IMongooseRepository,
} from './mongoose.repository.interface';

export abstract class BaseMongooseRepository<T> implements IMongooseRepository<T> {
  protected model: any;
  protected session?: ClientSession;

  constructor(private t: new () => T) {
    this.model = getModelForClass(t);
    this.model.schema.plugin(mongoosePaginate);
    this.model.schema.plugin(aggregatePaginate);
  }

  withTransaction(session?: ClientSession): BaseMongooseRepository<T> {
    const obj: BaseMongooseRepository<T> = Object.create(this);
    obj.session = session;
    return obj;
  }

  async count(): Promise<number> {
    const res: number = await this.model.count();
    return res;
  }

  async create(item: T): Promise<T> {
    const res: T[] = await this.model.create([item], { session: this.session, lean: true });

    const resCreate: any = res[0];

    return resCreate.toObject();
  }

  async createMany(items: T[]): Promise<T[]> {
    const res: T[] = await this.model.create(items, { session: this.session, lean: true });

    const resCreateMany = res.map((item: any) => {
      return item.toObject();
    });

    return resCreateMany;
  }

  async update(id: string, item: Partial<T>): Promise<T> {
    const res: T = await this.model.findByIdAndUpdate(id, item, { upsert: false, session: this.session });

    if (!res) {
      throw new EntityNotFoundError();
    }

    return await this.model.findById(id).lean();
  }

  async delete(id: string): Promise<void> {
    const res: T = await this.model.findByIdAndDelete(id, { session: this.session });

    if (!res) {
      throw new EntityNotFoundError();
    }
  }

  async deleteMany(ids: string[]): Promise<void> {
    await this.model.deleteMany({ _id: { $in: ids } }, { session: this.session });
  }

  async findAll(): Promise<T[]> {
    const res: T[] = await this.model.find({}).lean();
    return res;
  }

  async findWithQuery(query: any, sortQuery?: any): Promise<T[]> {
    let res: T[];

    if (sortQuery) {
      res = await this.model.find(query).sort(sortQuery).lean();
      return res;
    }

    res = await this.model.find(query).lean();

    return res;
  }

  async findOne(id: string): Promise<T> {
    const res: T = await this.model.findById(id).lean();

    // if (!res) {
    //   throw new EntityNotFoundError(`${id}`);
    // }

    return res;
  }

  async findOneByCode(code: string): Promise<T> {
    const res: T = await this.model.findOne({ code: code }).lean();
    return res;
  }

  async list(params: IMongoosePaginationOptions): Promise<IPagination<T>> {
    const query = search(params.search, params.searchFields);
    const options = {
      sort: params.sort,
      limit: params.pageSize,
      page: params.page,
      lean: true,
    };
    const customLabels = {
      totalDocs: 'total',
      docs: 'rows',
      limit: 'pageSize',
      page: 'page',
    };
    return this.model.paginate(query, { ...options, customLabels });
  }

  protected async listWithQuery(params: IMongoosePaginationOptions): Promise<IPagination<T>> {
    const options = {
      sort: params.sort,
      limit: params.pageSize,
      page: params.page,
      lean: true,
    };
    const customLabels = {
      totalDocs: 'total',
      docs: 'rows',
      limit: 'pageSize',
      page: 'page',
    };
    return this.model.paginate(params.query, { ...options, customLabels });
  }

  protected async listWithAggregate(params: IMongooseAggregatePaginationOptions): Promise<IPagination<T>> {
    const options = {
      sort: params.sort,
      limit: params.pageSize,
      page: params.page,
    };
    const customLabels = {
      totalDocs: 'total',
      docs: 'rows',
      limit: 'pageSize',
      page: 'page',
    };
    return this.model.paginate(params.aggregateQuery, { ...options, customLabels });
  }

  async remove(id: string): Promise<T> {
    return await this.model.findByIdAndRemove(id, { session: this.session });
  }

  async findAllByIds(ids: Uuid[]): Promise<T[]> {
    const query = {
      _id: { $in: ids },
    };
    return await this.model.find(query).lean();
  }
}

// Todo: validate fields
const search = (search?: string, searchFields?: string[]) => {
  if (search && searchFields?.length) {
    type QueryType = '$or' | '$and';

    type QueryNameFields = typeof searchFields[number];
    type RegexObject = {
      $regex: string;
      $options: 'i';
    };

    type ArrayRegex = Partial<Record<QueryNameFields, RegexObject | string>>[];
    const resultQuery: Partial<Record<QueryType, ArrayRegex>> = {};

    const queryOr: ArrayRegex = searchFields.map((fieldName: string) => {
      return {
        [fieldName]: {
          $regex: search,
          $options: 'i',
        },
      };
    });

    Object.assign(resultQuery, {
      $or: queryOr,
      $and: [{ active: true }],
    });

    return resultQuery;
  }
};
