import { Service as MoleculerService } from "moleculer";
import { Service } from "moleculer-decorators";
import { NestFactory } from "@nestjs/core";
import DbService from "moleculer-db";
import MongooseAdapter from "moleculer-db-adapter-mongoose";
import { SeedDbMixin, ChiefComplaintModel } from "platform-api-schema";
import { config } from "../../ever.config";
import { AppModule } from "./modules/app.module";
import { ChiefComplaintControllerMixin } from "./modules/chiefComplaint/chiefComplaint.controller.mixin";
import chiefComplaintSeedItems from "./modules/chiefComplaint/data/chiefComplaint.seed.json";
import { ChiefComplaintPopulateMixin } from "./modules/chiefComplaint/chiefComplaint.populate.mixin";

const SERVICE_NAME = "diagnostic";
@Service({
  name: `${SERVICE_NAME}`,
  mixins: [
    DbService,
    ChiefComplaintControllerMixin,

    SeedDbMixin([
      { seedModel: ChiefComplaintModel, seedItems: chiefComplaintSeedItems },
    ]),

    //populate
    ChiefComplaintPopulateMixin,
  ],
  adapter: new MongooseAdapter(config.mongoUri, { useUnifiedTopology: true }),
  model: DiagnosticSettingModel, // This is used for moleculer to connect to mngodb by using mongoose
  actions: {
    get: { visibility: "private" },
    list: { visibility: "private" },
    find: { visibility: "private" },
    count: { visibility: "private" },
    create: { visibility: "private" },
    insert: { visibility: "private" },
    update: { visibility: "private" },
    remove: { visibility: "private" },
  },
  settings: { rest: `${SERVICE_NAME}/` },
})
export default class DiagnosticService extends MoleculerService {
  private app: any;

  // Reserved for moleculer, fired when started
  public async started() {
    console.log(
      `=============== ${SERVICE_NAME} service is started =============== `
    );
  }

  // Reserved for moleculer, fired when created
  public async created() {
    console.log(
      `=============== ${SERVICE_NAME} service is created =============== `
    );
    this.app = await NestFactory.createApplicationContext(AppModule, {
      logger: ["error", "warn"],
    });
    console.log(
      `=============== ${SERVICE_NAME} DI container is created =============== `
    );
  }

  // Reserved for moleculer, fired when stopped
  public async stopped() {
    console.log(
      `=============== ${SERVICE_NAME} service is stopped =============== `
    );
  }
}
