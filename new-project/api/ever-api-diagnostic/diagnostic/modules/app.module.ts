import { Module } from "@nestjs/common";
import { ChiefComplaintModule } from "./chiefComplaint/chiefComplaint.module";

@Module({
  imports: [ChiefComplaintModule],
})
export class AppModule {}
