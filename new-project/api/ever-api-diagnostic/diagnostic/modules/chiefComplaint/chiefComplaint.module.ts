import { Module } from '@nestjs/common';
import { ChiefComplaintRepository } from './chiefComplaint.repository';
import { ChiefComplaintService } from './chiefComplaint.service';

@Module({
  imports: [],
  providers: [ChiefComplaintService, ChiefComplaintRepository],
  exports: [ChiefComplaintService],
})
export class ChiefComplaintModule {}
