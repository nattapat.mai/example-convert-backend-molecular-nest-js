import { ServiceSchema } from 'moleculer';
import { populateBase, getQueryPopulate } from 'platform-api-schema';

export const ChiefComplaintPopulateMixin: Partial<ServiceSchema> = {
  hooks: {
    after: {
      'chiefComplaint.*': async function (ctx, res) {
        let updateData = res;

        // hook after for only get method
        if (ctx.meta && ctx.meta?.reqMethod !== 'GET') {
          return res;
        }

        const populateDataByIds = getQueryPopulate(ctx);

        // base populate createdBy,updatedBy
        const basePopulate = [
          {
            populateKey: 'createdBy',
            fieldName: 'createdBy',
            actionApi: 'aaa.user.baseListByIds',
            fieldFilter: ['_id', 'fullName', 'username'],
          },
          {
            populateKey: 'updatedBy',
            fieldName: 'updatedBy',
            actionApi: 'aaa.user.baseListByIds',
            fieldFilter: ['_id', 'fullName', 'username'],
          },
        ];

        for (const populateData of basePopulate) {
          if (populateDataByIds.includes(populateData.populateKey)) {
            updateData = await populateBase(
              ctx,
              updateData,
              populateData.fieldName,
              populateData.actionApi,
              populateData.fieldFilter
            );
          }
        }

        return updateData;
      },
    },
  },
};
