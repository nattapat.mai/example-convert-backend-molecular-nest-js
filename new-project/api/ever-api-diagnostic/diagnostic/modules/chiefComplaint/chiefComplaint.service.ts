import mongoose from 'mongoose';
import { Inject, Injectable, Logger } from '@nestjs/common';
import { ChiefComplaint, IPagination, removeObjectValueUndefined, RequestContext, Uuid } from 'platform-api-schema';
import { REQUEST } from '@nestjs/core';
import { CreateChiefComplaintRequestDto } from './dto/createChiefComplaint.dto';
import { ListChiefComplaintRequestDto } from './dto/listChiefComplaint.dto';
import { ChiefComplaintRepository } from './chiefComplaint.repository';
import { UpdateChiefComplaintRequestDto } from './dto/updateChiefComplaint.dto';

@Injectable()
export class ChiefComplaintService {
  private readonly logger = new Logger(ChiefComplaintService.name);
  constructor(
    @Inject(REQUEST) private requestContext: RequestContext,
    private chiefComplaintRepository: ChiefComplaintRepository
  ) {}

  async findAllRoleByIds(ids: Uuid[]): Promise<ChiefComplaint[]> {
    return this.chiefComplaintRepository.findAllByIds(ids);
  }

  async findDataByEncounterId(encounterId: Uuid): Promise<ChiefComplaint[]> {
    const sort = {
      createdAt: -1,
    };
    return this.chiefComplaintRepository.findWithQuery(
      {
        encounterRef: encounterId,
        active: true,
      },
      sort
    );
  }

  async findDataByPatientId(patientId: Uuid): Promise<ChiefComplaint[]> {
    const sort = {
      createdAt: -1,
    };
    return this.chiefComplaintRepository.findWithQuery(
      {
        active: true,
        patientRef: patientId,
      },
      sort
    );
  }

  async getChiefComplaint(id: Uuid): Promise<ChiefComplaint> {
    this.logger.debug('get Encounter by ID...');
    return this.chiefComplaintRepository.findOne(id);
  }

  async listChiefComplaint(dto: ListChiefComplaintRequestDto): Promise<IPagination<ChiefComplaint>> {
    return this.chiefComplaintRepository.listChiefComplaint(dto.page, dto.pageSize, dto.sort, dto.search);
  }
  async createChiefComplaint(dto: CreateChiefComplaintRequestDto): Promise<ChiefComplaint> {
    this.logger.debug('create chiefComplaint...');
    // create ChiefComplaint object from DTO

    const newChiefComplaint: ChiefComplaint = {
      encounterRef: dto.encounterRef,
      patientRef: dto.patientId,
      text: dto.text,
      richText: dto.richText,
      remark: dto.remark,
      isFromDoctor: dto.isFromDoctor,
      formsValues: dto.formsValues,
      esiLevel: dto.esiLevel,
      createdBy: this.requestContext.user?._id,
      updatedBy: this.requestContext.user?._id,
    };

    const session = await mongoose.startSession();

    await session.startTransaction();

    try {
      const response = await this.chiefComplaintRepository.withTransaction(session).create(newChiefComplaint);
      await session.commitTransaction();
      return response;
    } catch (err) {
      await session.abortTransaction();
      throw err;
    } finally {
      session.endSession();
    }
  }

  async updateChiefComplaint(dto: UpdateChiefComplaintRequestDto): Promise<ChiefComplaint> {
    this.logger.debug('update chiefComplaint...');
    // update ChiefComplaint object from DTO
    const updateChiefComplaint: ChiefComplaint = {
      text: dto.text,
      richText: dto.richText,
      remark: dto.remark,
      active: dto.active,
      isFromDoctor: dto.isFromDoctor,
      formsValues: dto.formsValues,
      esiLevel: dto.esiLevel,
      updatedBy: this.requestContext.user?._id,
    } as ChiefComplaint;

    // update ChiefComplaint to DB
    return this.chiefComplaintRepository.update(dto._id, removeObjectValueUndefined(updateChiefComplaint));
  }
}
