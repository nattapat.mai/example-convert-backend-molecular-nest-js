import { Injectable } from "@nestjs/common";
import { ChiefComplaint } from "../../../../../data-model/diagnostic/chiefComplaint";
import { BaseMongooseRepository } from "../../../../../data-model/mongose/mongoose.repository.base";

@Injectable()
export class ChiefComplaintRepository extends BaseMongooseRepository<ChiefComplaint> {
  constructor() {
    super(ChiefComplaint);
  }

  async listChiefComplaint(
    page: number,
    pageSize: number,
    sort?: string,
    search?: string
  ): Promise<IPagination<ChiefComplaint>> {
    return this.list({
      page: page || 1,
      pageSize: pageSize || 10,
    });
  }
}
