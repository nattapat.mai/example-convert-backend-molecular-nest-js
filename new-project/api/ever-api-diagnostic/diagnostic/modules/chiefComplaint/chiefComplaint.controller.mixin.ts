import { Context, ServiceSchema, Errors } from 'moleculer';
import {
  CHANGED_PARAM_NAME,
  COMPENSATE_SUFFIX,
  ChiefComplaint,
  IPagination,
  UnexpectedBusinessLogicError,
  Uuid,
  UnexpectedClientApiError,
  Meta,
  getByIdAction,
  ListByIdsAction,
} from 'platform-api-schema';
import { getSchema } from 'fastest-validator-decorators';
import { ContextIdFactory } from '@nestjs/core';
import { ChiefComplaintService } from './chiefComplaint.service';
import { ListChiefComplaintRequestDto, ListChiefComplaintResponseDto } from './dto/listChiefComplaint.dto';
import { CreateChiefComplaintRequestDto, CreateChiefComplaintResponseDto } from './dto/createChiefComplaint.dto';
import { UpdateChiefComplaintRequestDto, UpdateChiefComplaintResponseDto } from './dto/updateChiefComplaint.dto';
import { GetChiefComplaintRequestDto, GetChiefComplaintResponseDto } from './dto/getChiefComplaint.dto';
import {
  AllChiefComplaintByPatientIdRequestDto,
  AllChiefComplaintByPatientIdResponseDto,
} from './dto/allChiefComplaintByPatientId.dto';

const RESOURCE_NAME = 'chiefComplaint';
export const ChiefComplaintControllerMixin: Partial<ServiceSchema> = {
  actions: {
    [`${RESOURCE_NAME}.baseGetById`]: getByIdAction<ChiefComplaint>(RESOURCE_NAME, ChiefComplaintService) as any,
    [`${RESOURCE_NAME}.baseListByIds`]: ListByIdsAction<ChiefComplaint>(RESOURCE_NAME, ChiefComplaintService) as any,

    // READ BY GROUP
    [`${RESOURCE_NAME}.findAllByIds`]: {
      async handler(ctx: Context<Uuid[], Meta>): Promise<ChiefComplaint[]> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        return await chiefComplaintService.findAllRoleByIds(ctx.params);
      },
    },

    // READ BY GROUP
    [`${RESOURCE_NAME}.findDataByEncounterId`]: {
      async handler(ctx: Context<{ encounterId: string }, Meta>): Promise<{ data: ChiefComplaint[] }> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        const res = await chiefComplaintService.findDataByEncounterId(ctx.params.encounterId);

        return { data: res };
      },
    },

    // READ BY GROUP
    [`${RESOURCE_NAME}.findAllByPatientId`]: {
      rest: `GET /${RESOURCE_NAME}s/findAllByPatientId/:patientId`,
      params: getSchema(AllChiefComplaintByPatientIdRequestDto),
      async handler(
        ctx: Context<AllChiefComplaintByPatientIdRequestDto, Meta>
      ): Promise<AllChiefComplaintByPatientIdResponseDto> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        const encounters: ChiefComplaint[] = await chiefComplaintService.findDataByPatientId(ctx.params.patientId);
        const res: AllChiefComplaintByPatientIdResponseDto = {
          data: encounters,
        };

        return res;
      },
    },
    // READ ALL
    [`${RESOURCE_NAME}.list`]: {
      rest: `GET /${RESOURCE_NAME}s`,
      params: getSchema(ListChiefComplaintRequestDto),
      async handler(ctx: Context<ListChiefComplaintRequestDto, Meta>): Promise<ListChiefComplaintResponseDto> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        const chiefComplaints: IPagination<ChiefComplaint> = await chiefComplaintService.listChiefComplaint(ctx.params);

        const res: ListChiefComplaintResponseDto = {
          data: {
            ...chiefComplaints,
            rows: chiefComplaints.rows.map((chiefData) => {
              return {
                ...chiefData,
              };
            }),
          },
        };
        return res;
      },
    },
    [`${RESOURCE_NAME}.get`]: {
      rest: `GET /${RESOURCE_NAME}s/:_id`,
      params: getSchema(GetChiefComplaintRequestDto),
      async handler(ctx: Context<GetChiefComplaintRequestDto, Meta>): Promise<GetChiefComplaintResponseDto> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        const encounter: ChiefComplaint = await chiefComplaintService.getChiefComplaint(ctx.params._id);
        const res: GetChiefComplaintResponseDto = {
          data: encounter,
        };

        return res;
      },
    },
    [`${RESOURCE_NAME}.create`]: {
      rest: `POST /${RESOURCE_NAME}s`,
      cache: false,
      saga: {
        compensation: {
          action: `${RESOURCE_NAME}.${COMPENSATE_SUFFIX}`,
          params: [CHANGED_PARAM_NAME],
        },
      },
      params: getSchema(CreateChiefComplaintRequestDto),
      async handler(ctx: Context<CreateChiefComplaintRequestDto, Meta>): Promise<CreateChiefComplaintResponseDto> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        try {
          const resChiefComplaint: ChiefComplaint = await chiefComplaintService.createChiefComplaint({ ...ctx.params });
          const res: CreateChiefComplaintResponseDto = {
            data: resChiefComplaint,
          };

          return res;
        } catch (err) {
          if (err instanceof Errors.MoleculerError) {
            throw err;
          } else {
            throw new UnexpectedBusinessLogicError(`Unexpected business logic error on create of ${RESOURCE_NAME}`);
          }
        }
      },
    },
    [`${RESOURCE_NAME}.update`]: {
      cache: false,
      saga: {
        compensation: {
          action: `${RESOURCE_NAME}.${COMPENSATE_SUFFIX}`,
          params: [CHANGED_PARAM_NAME],
        },
      },
      rest: `PUT /${RESOURCE_NAME}s/:_id`,
      params: getSchema(UpdateChiefComplaintRequestDto),
      async handler(ctx: Context<UpdateChiefComplaintRequestDto, Meta>): Promise<UpdateChiefComplaintResponseDto> {
        const contextId = ContextIdFactory.create();
        const requestContext = { user: ctx.meta.user };
        this.app.registerRequestByContextId(requestContext, contextId);
        const chiefComplaintService: ChiefComplaintService = await this.app.resolve(ChiefComplaintService, contextId);

        try {
          const ChiefComplaint: ChiefComplaint = await chiefComplaintService.updateChiefComplaint({ ...ctx.params });
          const res: UpdateChiefComplaintResponseDto = {
            data: ChiefComplaint,
          };
          return res;
        } catch (err) {
          if (err instanceof Errors.MoleculerError) {
            throw err;
          } else {
            throw new UnexpectedClientApiError(err);
          }
        }
      },
    },
  },
};
