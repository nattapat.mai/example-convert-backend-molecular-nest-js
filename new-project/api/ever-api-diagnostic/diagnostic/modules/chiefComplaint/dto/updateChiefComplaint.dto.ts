import { Schema, UUID, String, Boolean, Array } from 'fastest-validator-decorators';
import { BaseApiRequest, BaseApiResponse, ICreateRequest, IResponse, ChiefComplaint, Uuid } from 'platform-api-schema';

@Schema()
export class UpdateChiefComplaintRequestDto extends BaseApiRequest implements ICreateRequest {
  @UUID()
  _id!: Uuid;

  @Boolean({ optional: true })
  active?: boolean;

  @String({ optional: true })
  text?: string;

  @String({ optional: true })
  richText?: string;

  @String({ optional: true })
  remark?: string;

  @Boolean({ optional: true })
  isFromDoctor?: boolean;

  @Array({ optional: true })
  formsValues?: any[];

  @String({ optional: true })
  esiLevel?: string;
}

export class UpdateChiefComplaintResponseDto extends BaseApiResponse implements IResponse {
  data!: ChiefComplaint;
}
