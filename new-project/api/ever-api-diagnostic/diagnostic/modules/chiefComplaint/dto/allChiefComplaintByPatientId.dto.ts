import { Schema, UUID } from 'fastest-validator-decorators';
import { BaseApiRequest, BaseApiResponse, IGetRequest, IResponse, ChiefComplaint, Uuid } from 'platform-api-schema';

@Schema()
export class AllChiefComplaintByPatientIdRequestDto extends BaseApiRequest implements IGetRequest {
  @UUID()
  patientId!: Uuid;
}

export class AllChiefComplaintByPatientIdResponseDto extends BaseApiResponse implements IResponse {
  data!: ChiefComplaint[];
}
