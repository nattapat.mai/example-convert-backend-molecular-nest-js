import { BaseApiResponse, IPagination, IPaginationListRequest, IResponse, ChiefComplaint } from 'platform-api-schema';
import { Schema, Number, String, Array } from 'fastest-validator-decorators';

@Schema()
export class ListChiefComplaintRequestDto implements IPaginationListRequest {
  @Number({ min: 1 })
  page!: number;

  @Number({ min: 1 })
  pageSize!: number;

  @String({ optional: true })
  sort?: string;

  @String({ optional: true })
  search?: string;

  @Array({ items: 'string', optional: true })
  searchFields?: string[];
}

export class ListChiefComplaintResponseDto extends BaseApiResponse implements IResponse {
  data!: IPagination<ChiefComplaint>;
}
