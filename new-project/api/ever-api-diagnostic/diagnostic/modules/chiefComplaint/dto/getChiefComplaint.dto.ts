import { Schema, UUID } from 'fastest-validator-decorators';
import { BaseApiRequest, BaseApiResponse, IGetRequest, IResponse, ChiefComplaint, Uuid } from 'platform-api-schema';

@Schema()
export class GetChiefComplaintRequestDto extends BaseApiRequest implements IGetRequest {
  @UUID()
  _id!: Uuid;
}

export class GetChiefComplaintResponseDto extends BaseApiResponse implements IResponse {
  data!: ChiefComplaint;
}
