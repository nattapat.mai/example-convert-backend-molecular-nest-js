import { Schema, UUID, String, Boolean, Array } from 'fastest-validator-decorators';
import { BaseApiRequest, BaseApiResponse, ICreateRequest, IResponse, ChiefComplaint, Uuid } from 'platform-api-schema';

@Schema()
export class CreateChiefComplaintRequestDto extends BaseApiRequest implements ICreateRequest {
  @UUID()
  encounterRef!: Uuid;

  @UUID()
  patientId!: Uuid;

  @String({ optional: true })
  text?: string;

  @String({ optional: true })
  remark?: string;

  @String()
  richText!: string;

  @Boolean({ optional: true })
  isFromDoctor?: boolean;

  @Array({ optional: true })
  formsValues?: any[];

  @String({ optional: true })
  esiLevel?: string;
}

export class CreateChiefComplaintResponseDto extends BaseApiResponse implements IResponse {
  data!: ChiefComplaint;
}
