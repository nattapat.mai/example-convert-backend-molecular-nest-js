import { modelOptions, prop, Ref } from '@typegoose/typegoose';
import { Encounter } from 'platform-api-administration';
import { EntityBase, Uuid, validateWith } from 'platform-api-common';

@modelOptions({
  schemaOptions: {
    collation: { locale: 'en' },
    collection: 'chief_complaint',
  },
})
export class ChiefComplaint extends EntityBase {
  @prop({ required: true, ref: 'Encounter', type: () => String, validate: validateWith('encounters.get') })
  encounter!: Ref<Encounter, Uuid>;

  @prop({ required: true })
  text!: string;
}
