import { ServiceSchema } from 'moleculer';
import { DbServiceMixin } from 'platform-api-common';
import { ChiefComplaintModel } from '../entity';

export const ChiefComplaintServiceBaseMixin: Partial<ServiceSchema> = {
  mixins: [DbServiceMixin],
  model: ChiefComplaintModel,
  settings: {
    populates: {
      encounter: 'encounters.get',
    },
  },
  actions: {
    create: {
      visibility: 'public',
    },
    insert: {
      visibility: 'public',
    },
    update: {
      visibility: 'public',
    },
    remove: {
      visibility: 'public',
    },
  },
};
