import { Service as MoleculerService } from "moleculer";
import { HookAuditLogMixin } from "platform-api-common";
import { Service } from "moleculer-decorators";
import {
  GetChiefComplaintMixin,
  UpdateChiefComplaintMixin,
  DeleteChiefComplaintMixin,
} from "../interactor";

@Service({
  name: "BffWeb_Diagnostics_PatientProfile",
  mixins: [
    GetChiefComplaintMixin,
    UpdateChiefComplaintMixin,
    DeleteChiefComplaintMixin,
  ],
  settings: { rest: "patient-profile/" },
})
export default class PatientProfilePageBffWebService extends MoleculerService {}
