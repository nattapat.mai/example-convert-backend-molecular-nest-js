import { Uuid } from 'platform-api-common';

export interface IBffUpdateChiefComplaintRequest {
  encounterId: Uuid;
  text: string;
}
