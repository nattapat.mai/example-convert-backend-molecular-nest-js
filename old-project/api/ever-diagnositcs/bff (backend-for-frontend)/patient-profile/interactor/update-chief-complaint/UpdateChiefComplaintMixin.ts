import { Context, ServiceSchema } from 'moleculer';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IBffUpdateChiefComplaintRequest } from './IBffUpdateChiefComplaintRequest';
import { IBffUpdateChiefComplaintResponse } from './IBffUpdateChiefComplaintResponse';

export const UpdateChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    updateChiefComplaint: {
      rest: 'POST /encounter/:encounterId/update-chief-complaint',
      params: {
        text: { type: 'string' },
      },
      async handler(ctx: Context<IBffUpdateChiefComplaintRequest>): Promise<IBffUpdateChiefComplaintResponse> {
        const chiefComplaint: ChiefComplaint = await ctx.call('chiefComplaints.putChiefComplaint', {
          encounterId: ctx.params.encounterId,
          text: ctx.params.text,
        });

        return {
          text: chiefComplaint.text,
        };
      },
    },
  },
};
