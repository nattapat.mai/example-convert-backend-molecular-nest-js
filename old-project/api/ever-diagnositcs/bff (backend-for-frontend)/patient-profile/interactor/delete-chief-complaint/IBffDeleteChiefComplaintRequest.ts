import { Uuid } from 'platform-api-common';

export interface IBffDeleteChiefComplaintRequest {
  encounterId: Uuid;
}
