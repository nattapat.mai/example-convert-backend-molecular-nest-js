import { Context, ServiceSchema } from 'moleculer';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IBffDeleteChiefComplaintRequest } from './IBffDeleteChiefComplaintRequest';
import { IBffDeleteChiefComplaintResponse } from './IBffDeleteChiefComplaintResponse';

export const DeleteChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    deleteChiefComplaint: {
      rest: 'DELETE /encounter/:encounterId/delete-chief-complaint',
      params: {
        encounterId: { type: 'uuid' },
      },
      async handler(ctx: Context<IBffDeleteChiefComplaintRequest>): Promise<IBffDeleteChiefComplaintResponse> {
        const chiefComplaint: ChiefComplaint = await ctx.call('chiefComplaints.deleteChiefComplaint', {
          encounterId: ctx.params.encounterId,
        });

        return {
          text: chiefComplaint.text,
        };
      },
    },
  },
};
