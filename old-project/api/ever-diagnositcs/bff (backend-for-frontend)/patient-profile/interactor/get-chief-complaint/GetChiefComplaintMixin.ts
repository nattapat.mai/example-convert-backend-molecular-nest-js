import { Context, ServiceSchema } from 'moleculer';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IBffGetChiefComplaintRequest } from './IBffGetChiefComplaintRequest';
import { IBffGetChiefComplaintResponse } from './IBffGetChiefComplaintResponse';

export const GetChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    getChiefComplaint: {
      rest: 'GET /encounter/:encounterId/get-chief-complaint',
      async handler(ctx: Context<IBffGetChiefComplaintRequest>): Promise<IBffGetChiefComplaintResponse> {
        const encounterId = ctx.params.encounterId;

        const chiefComplaint: ChiefComplaint = await ctx.call('chiefComplaints.getChiefComplaint', { encounterId });

        return {
          text: chiefComplaint.text,
        };
      },
    },
  },
};
