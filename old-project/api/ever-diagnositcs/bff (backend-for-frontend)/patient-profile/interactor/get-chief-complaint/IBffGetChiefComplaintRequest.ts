import { Uuid } from 'platform-api-common';

export interface IBffGetChiefComplaintRequest {
  encounterId: Uuid;
}
