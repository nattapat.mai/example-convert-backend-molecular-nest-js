import { Context, ServiceSchema, Errors } from 'moleculer';
import { Encounter, EncounterStatus } from 'platform-api-administration';
import { EntityNotFoundError } from 'platform-api-common';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IPutChiefComplaintRequest } from './IPutChiefComplaintRequest';
import { IPutChiefComplaintResponse } from './IPutChiefComplaintResponse';

export const PutChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    putChiefComplaint: {
      rest: 'PUT /:encounterId/chiefComplaint',
      params: {
        text: { type: 'string', empty: false, trim: true },
      },
      async handler(ctx: Context<IPutChiefComplaintRequest>): Promise<IPutChiefComplaintResponse> {
        const encounterId = ctx.params.encounterId;
        const text = ctx.params.text;

        const encounter: Encounter = await ctx.call('encounters.get', { id: encounterId });

        if (!encounter.active) {
          throw new EntityNotFoundError();
        }

        const chiefComplaints: ChiefComplaint[] = await ctx.call('chiefComplaints.find', {
          query: {
            encounter: encounterId,
            active: true,
          },
          limit: 1,
        });

        if (chiefComplaints.length === 0) {
          if (encounter.status === EncounterStatus.FINISHED) {
            throw new Errors.MoleculerClientError(`Encounter Already Discharge`);
          }

          return await ctx.call('chiefComplaints.create', {
            encounter: encounterId,
            text,
          });
        }

        return ctx.call('chiefComplaints.update', { id: chiefComplaints[0]._id, text });
      },
    },
  },
};
