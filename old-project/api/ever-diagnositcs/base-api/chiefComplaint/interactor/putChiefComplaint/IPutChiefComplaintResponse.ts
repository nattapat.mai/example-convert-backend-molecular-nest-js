import { ChiefComplaint } from 'platform-api-diagnostics';

export type IPutChiefComplaintResponse = ChiefComplaint;
