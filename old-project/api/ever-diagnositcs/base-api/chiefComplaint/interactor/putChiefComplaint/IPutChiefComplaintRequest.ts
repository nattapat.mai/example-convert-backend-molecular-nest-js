import { Uuid } from 'platform-api-common';

export interface IPutChiefComplaintRequest {
  encounterId: Uuid;
  text: string;
}
