import { Uuid } from 'platform-api-common';

export interface IGetChiefComplaintRequest {
  encounterId: Uuid;
}
