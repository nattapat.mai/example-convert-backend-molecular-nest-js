import { ChiefComplaint } from 'platform-api-diagnostics';

export type IGetChiefComplaintResponse = ChiefComplaint;
