import { Context, ServiceSchema } from 'moleculer';
import { Encounter } from 'platform-api-administration';
import { EntityNotFoundError } from 'platform-api-common';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IGetChiefComplaintRequest } from './IGetChiefComplaintRequest';
import { IGetChiefComplaintResponse } from './IGetChiefComplaintResponse';

export const GetChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    getChiefComplaint: {
      rest: 'GET /encounter/:encounterId/get-chief-complaint',
      async handler(ctx: Context<IGetChiefComplaintRequest>): Promise<IGetChiefComplaintResponse> {
        const encounterId = ctx.params.encounterId;

        const encounter: Encounter = await ctx.call('encounters.get', { id: encounterId });

        if (!encounter.active) {
          throw new EntityNotFoundError();
        }

        const chiefComplaints: ChiefComplaint[] = await ctx.call('chiefComplaints.find', {
          query: {
            encounter: encounterId,
            active: true,
          },
          limit: 1,
        });

        if (chiefComplaints.length === 0) {
          throw new EntityNotFoundError();
        }

        return chiefComplaints[0];
      },
    },
  },
};
