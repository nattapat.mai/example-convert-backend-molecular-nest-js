import { Uuid } from 'platform-api-common';

export interface IDeleteChiefComplaintRequest {
  encounterId: Uuid;
}
