import { ChiefComplaint } from 'platform-api-diagnostics';

export type IDeleteChiefComplaintResponse = ChiefComplaint;
