import { Context, ServiceSchema } from 'moleculer';
import { Encounter } from 'platform-api-administration';
import { EntityNotFoundError } from 'platform-api-common';
import { ChiefComplaint } from 'platform-api-diagnostics';
import { IDeleteChiefComplaintRequest } from './IDeleteChiefComplaintRequest';
import { IDeleteChiefComplaintResponse } from './IDeleteChiefComplaintResponse';

export const DeleteChiefComplaintMixin: Partial<ServiceSchema> = {
  actions: {
    deleteChiefComplaint: {
      rest: 'DELETE /:encounterId/chiefComplaint',
      params: {
        encounterId: { type: 'uuid' },
      },
      async handler(ctx: Context<IDeleteChiefComplaintRequest>): Promise<IDeleteChiefComplaintResponse> {
        const encounterId = ctx.params.encounterId;

        const encounter: Encounter = await ctx.call('encounters.get', { id: encounterId });

        if (!encounter.active) {
          throw new EntityNotFoundError();
        }

        const chiefComplaints: ChiefComplaint[] = await ctx.call('chiefComplaints.find', {
          query: {
            encounter: encounterId,
            active: true,
          },
          limit: 1,
        });

        if (chiefComplaints.length === 0) {
          throw new EntityNotFoundError();
        }

        const chiefComplaint = chiefComplaints[0];

        return await ctx.call('chiefComplaints.update', { id: chiefComplaint._id, active: false });
      },
    },
  },
};
