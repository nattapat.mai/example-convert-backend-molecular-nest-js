import { Service as MoleculerService } from "moleculer";
import { Service } from "moleculer-decorators";
import { ChiefComplaintServiceBaseMixin } from "platform-api-diagnostics";
import {
  DeleteChiefComplaintMixin,
  GetChiefComplaintMixin,
  PutChiefComplaintMixin,
} from "../interactor";

@Service({
  name: "chiefComplaints",
  mixins: [
    ChiefComplaintServiceBaseMixin,
    DeleteChiefComplaintMixin,
    GetChiefComplaintMixin,
    PutChiefComplaintMixin,
  ],
})
export default class ChiefComplaintService extends MoleculerService {}
